import pandas as pd
import geopandas
import shapely
import shapefile
import dash_table as dt
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
from dash.exceptions import PreventUpdate

from dash.dependencies import Input, Output, State
from dash_setup import app
from util import graph_themes
from sources import us_covid_data, county_covid_data

us_state_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'American Samoa': 'AS',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District of Columbia': 'DC',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Guam': 'GU',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands': 'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY'
}

colorscale = [
    'rgb(193, 193, 193)',
    'rgb(239,239,239)',
    'rgb(195, 196, 222)',
    'rgb(144,148,194)',
    'rgb(101,104,168)',
    'rgb(65, 53, 132)'
]


def abbrevState(state):
    if state in us_state_abbrev:
        return us_state_abbrev[state]
    else:
        return state

stats_columns = {
    'icu_patients': 'ICU Patients',
    'hosp_patients': 'Hospital Patients',
    'new_tests': 'New Tests',
    'total_tests': 'Total Tests',
    'positive_rate': 'Positive Rate'
}

table_columns = sorted(list(county_covid_data.columns[-8:]), reverse=True)

covidStats_layout = html.Div([
    dbc.Row(
        dbc.Col(
            children=[

                dbc.Card(id='stat-card',body=True, style={'margin-top': 10}, children=[
                    dbc.Row(
                        [html.H2("US COVID-19 Cases Statistics"), ],
                        justify="center"
                    ),
                    dcc.Dropdown(
                        id='y-axis',
                        options=[
                            {'label': stats_columns[col], 'value': col} for col in stats_columns.keys()
                        ],
                        value='positive_rate'
                    ),
                    dcc.Graph(id="US graph"),
                ]),

                dbc.Card(id='state-covid-card',body=True, style={'margin-top': '4vh'}, children=[
                    dbc.Row(
                        [html.H2("State COVID-19 Cases"), ],
                        justify="center"
                    ),
                    dcc.Dropdown(
                        id='stateSelection',
                        options=[
                            {'label': '{}'.format(i), 'value': i} for i in county_covid_data['Province_State'].unique()
                        ],
                        value='IL'
                    ),

                    dcc.Graph(id='choropleth'),
                    html.P("Confirmed Cases over the last 7 days by State"),
                    dt.DataTable(
                        id='stateTable',
                        columns=[{"name": i, "id": i} for i in table_columns],
                        page_size=15,
                        style_as_list_view=True,
                        style_header={
                            'backgroundColor': 'white',
                            'fontWeight': 'bold'
                        },
                        style_cell={
                            'padding': '5px',
                            'overflow': 'hidden',
                            'textOverflow': 'ellipsis',
                            'maxWidth': 0,
                            'whiteSpace': 'normal',
                            'height': 'auto',
                            'textAlign': 'left',
                            'boxShadow': '0 0'
                        },
                    ),

                ]

                )

            ],
            width={"size": 10, "offset": 1}
        )
    ),

],
    style={'marginTop': 100}
)


@app.callback(
    Output('US graph', 'figure'),
    Input('y-axis', 'value'))
def display_graph(yaxis):
    if not yaxis:
        raise PreventUpdate

    labels = {**stats_columns, 'date': 'Date'}
    fig = px.line(us_covid_data, 'date', yaxis, title="US Data", labels=labels)
    fig.update_layout(
        plot_bgcolor='white',
        paper_bgcolor='white',
        font_color='#2a3f5f'
    )
    return fig


@app.callback(
    Output('choropleth', 'figure'),
    Input('stateSelection', 'value'),
)
def display_choropleth(state):
    if not state:
        raise PreventUpdate

    scope = [state]
    df_State = county_covid_data[county_covid_data['Province_State'].isin(
        scope)]
    values = df_State['3/1/21'].tolist()
    fips = df_State['FIPS'].tolist()

    fig = ff.create_choropleth(
        fips=fips,
        values=values,
        scope=scope,
        round_legend_values=True,
        simplify_county=0,
        simplify_state=0,
        legend_title='Confirmed cases per county as of 3/1/21',
        title=list(us_state_abbrev.keys())[
            list(us_state_abbrev.values()).index(state)]
    )
    fig.update_layout(
        plot_bgcolor='white',
        paper_bgcolor='white',
        font_color='#2a3f5f'
    )
    return fig


@app.callback(
    Output('stateTable', 'data'),
    Input('stateSelection', 'value')
)
def update_table(state):
    scope = [state]
    df_State = county_covid_data[county_covid_data['Province_State'].isin(scope)]
    data = df_State[table_columns].to_dict('records')
    return data

@app.callback(Output('stat-card', 'style'),
              Input('toggle-theme', 'value'),
              State('stat-card', 'style'))
def toggle_stat_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style

@app.callback(Output('state-covid-card', 'style'),
              Input('toggle-theme', 'value'),
              State('state-covid-card', 'style'))
def toggle_trend_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style