# VaxTrax
A COVID-19 data dashboard for viewing information about COVID-19 vaccinations and other related statistics

## Local Installation
1. Install required Python dependencies: `pip install -r requirements.txt`
2. [Download](https://chromedriver.chromium.org/downloads) Chromedriver and add its executable to PATH
3. Download and Proprocess COVID data: `python download.py`

## Descriptions
- dash_setup.py creates the initial dash app
- application.py runs the dash app and determines what page should be shown

## Citations
Layout and callbacks for the citations page go in citations
