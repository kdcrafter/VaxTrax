import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
import pandas as pd
import plotly.express as px
import plotly.figure_factory as ff
import dash_table as dt

from dash_setup import app

# Race group graph
races = ('White', 'Hispanic/Latino', 'Black', 'Asian', ' American Indian/Alaska Native',
         'Native Hawaiian or Other Pacific Islander', 'Multiple/Other, non-Hispanic')
race_numbers = [20190226, 2228890, 2193702, 1186572, 453574, 73825, 2972142]
race_fig = go.Figure(data=[go.Pie(
    title='Percentage of Each Race/Ethnicity Vaccinated', labels=races, values=race_numbers)])

# Race Population Demographics
race_population_percentages = [60.1, 18.5, 12.2, 5.6, 0.7, 0.2, 2.8]
race_population_fig = go.Figure(data=[go.Pie(
    title='U.S. Population Race Demographics (Data as of 2019)', labels=races, values=race_population_percentages)])

# Gender group one dose graph
genders = ('Female', 'Male')
gender_one_dose_numbers = [49661930, 39325360]
gender_one_dose_fig = go.Figure(data=[go.Pie(
    title='Gender of People With At Least One Dose Administered', labels=genders, values=gender_one_dose_numbers)])

# Gender group fully vaccinated graph
gender_fully_vaccinated_numbers = [28754391, 21291385]
gender_fully_vaccinated_fig = go.Figure(data=[go.Pie(
    title='Gender of People Fully Vaccinated', labels=genders, values=gender_fully_vaccinated_numbers)])

# Gender Population Demographics
gender_pop_numbers = [163000000, 156000000]
gender_population_fig = go.Figure(data=[go.Pie(
    title='U.S. Population Gender Demographics (Data as of 2019)', labels=genders, values=gender_pop_numbers)])


# Age group graph
age_labels = '<18', '18–29', '30–39', '40–49', '50–64', '65–74', '75+'
age_percentages = [0.1, 7.3, 9.8, 16.8, 10.6, 20.7, 23.1]

age_fig = go.Figure(data=[go.Pie(
    title='Percentage of Each Age Group Vaccinated', labels=age_labels, values=age_percentages)])

# Age Population Demographics
age_population_labels = '<18', '19–25', '26-34', '35-54', '55–64', '65+'
age_population_percentages = [23.6, 8.7, 12.5, 25.5, 13.2, 16.5]

age_population_fig = go.Figure(data=[go.Pie(
    title='U.S. Population Age Demographics (Data as of 2019)', labels=age_population_labels, values=age_population_percentages)])

# Age group fully vaccinated vs one dose analysis
data = [['One Dose', 'Total', 97593290],
        ['One Dose', '>=18', 97226718],
        ['One Dose', '>=65', 40218262],
        ['Fully Vaccinated', 'Total', 54607041],
        ['Fully Vaccinated', '>=18', 54514865],
        ['Fully Vaccinated', '>=65', 27762018]]


df = pd.DataFrame(
    data, columns=['Vaccination Status', 'Age Group', 'Number of People'])

age_analysis_fig = px.bar(df, x="Age Group", y="Number of People",
                          color="Vaccination Status", title="Age Group One Dose vs. Fully Vaccinated Analysis")

# Gender group fully vaccinated vs one dose analysis
data = [['One Dose', 'Female', 49661930],
        ['One Dose', 'Male', 39325360],
        ['Fully Vaccinated', 'Female', 28754391],
        ['Fully Vaccinated', 'Male', 21291385]]


df = pd.DataFrame(
    data, columns=['Vaccination Status', 'Gender', 'Number of People'])

gender_analysis_fig = px.bar(df, x="Gender", y="Number of People", color="Vaccination Status",
                             title="Gender Group One Dose vs. Fully Vaccinated Analysis")

# Increased Risk Groups
increased_risk_groups = [['People with Underlying Medical Conditions'],
                         ['Pregnant Women'],
                         ['Correctional Facilities'],
                         ['People with Cancer'],
                         ['People with Chronic Kidney Disease'],
                         ['People with Chronic Lung Diseases'],
                         ['People with Dementia or Other Neurological Conditions'],
                         ['People with Diabetes (type 1 or type 2)'],
                         ['People with Down Syndrome'],
                         ['People with Heart Conditions'],
                         ['People with HIV Infections'],
                         ['People in Immunocomprimised States'],
                         ['People with Liver Disease'],
                         ['Overweight and Obese People'],
                         ['People with Sickle Cell Disease'],
                         ['Smokers']]

increased_risk_df = pd.DataFrame(
    increased_risk_groups, columns=['People at Increased Risk of COVID-19'])


increased_risk_groups_fig =  dt.DataTable(
    id='increased_risk_groups',
    columns=[{"name": i, "id": i} for i in increased_risk_df.columns],
    data=increased_risk_df.to_dict('records'),
    style_as_list_view=True,
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold'
    },
    style_cell={
        'padding': '5px',
        'overflow': 'hidden',
        'textOverflow': 'ellipsis',
        'maxWidth': 0,
        'whiteSpace': 'normal',
        'height': 'auto',
        'textAlign': 'left',
        'boxShadow': '0 0'
    },
)

#increased_risk_groups_fig = ff.create_table(increased_risk_groups)

# Pregnant Women Age Graph
pregnant_women_age_groups = ['<15', '15-19', '20-24',
                             '25-29', '30-34', '35-39', '40-44', '45-49', '50-54']
pregnant_women_age_values = [201, 4276, 17561,
                             25883, 22933, 11656, 3137, 698, 532]

pregnant_women_age_fig = go.Figure(data=[go.Pie(
    title='Pregnant women with COVID-19 by age as of April 2021', labels=pregnant_women_age_groups, values=pregnant_women_age_values)])

# Pregnant Women Race Graph
pregnant_women_race_groups = ['Hispanic/Latino', 'Asian', 'Black', 'White', 'Multiple/ Other Race, Non-Hispanic']
pregnant_women_race_values = [31725, 2275, 9564, 27094, 4672]

pregnant_women_race_fig=go.Figure(data=[go.Pie(title = 'Pregnant women with COVID-19 by race as of April 2021', labels=pregnant_women_race_groups, values=pregnant_women_race_values)])

# Pregnant Women Statistics
pregnant_women_stats = [['Total Cases', '86,877'],
                            ['Total Deaths', '97'],
                            ['Hospitalized Cases', '14,745']]

#pregnant_women_stats_fig = ff.create_table(pregnant_women_stats)


pregnant_women_df = pd.DataFrame(
    pregnant_women_stats, columns=['Pregnant Women with COVID-19 (January 22, 2020 - April 19, 2021)', 'Cases'])


pregnant_women_table =  dt.DataTable(
    id='pregnant_women_table',
    columns=[{"name": i, "id": i} for i in pregnant_women_df.columns],
    data=pregnant_women_df.to_dict('records'),
    style_as_list_view=True,
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold'
    },
    style_cell={
        'padding': '5px',
        'overflow': 'hidden',
        'textOverflow': 'ellipsis',
        'maxWidth': 0,
        'whiteSpace': 'normal',
        'height': 'auto',
        'textAlign': 'left',
        'boxShadow': '0 0'
    },
)




# Correctional Facilities
correctional_facilities_stats = [['Affected Facilities', 1597],
               ['Total Cases', 510544],
               ['Total Resident Cases', 413837],
               ['Total Staff Cases', 96707],
               ['Total Deaths', 2646],
               ['Total Resident Deaths', 2487],
               ['Total Staff Deaths', 159]]


correctional_facilities_df = pd.DataFrame(
    correctional_facilities_stats, columns=['Correctional Facilities Demographics', 'Cases'])


correctional_facilities_table =  dt.DataTable(
    id='correctional_facilities_tables',
    columns=[{"name": i, "id": i} for i in correctional_facilities_df.columns],
    data=correctional_facilities_df .to_dict('records'),
    style_as_list_view=True,
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold'
    },
    style_cell={
        'padding': '5px',
        'overflow': 'hidden',
        'textOverflow': 'ellipsis',
        'maxWidth': 0,
        'whiteSpace': 'normal',
        'height': 'auto',
        'textAlign': 'left',
        'boxShadow': '0 0'
    },
)

#correctional_facilities_fig = ff.create_table(correctional_facilities_stats)

demographics_layout = html.Div([

    dbc.Row(
        dbc.Col(
            children=[dbc.Card(id = 'demographics_card',style={'margin-top': 10}, children=[

                dbc.Row(
                    [html.H2("Vaccination Demographics"), ],
                    justify="center",

                ),

                dcc.Tabs(id='demographics-tabs', value='tab-1', style={'margin-top': 20}, children=[
                    dcc.Tab(id = 'race-tab', label='Race', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-1'),
                    dcc.Tab(id='age-tab', label='Age', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-2'),
                    dcc.Tab(id='gender-tab', label='Gender', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-3'),
                    dcc.Tab(id='gender-analysis-tab',label='Gender Analysis', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-4'),
                    dcc.Tab(id='age-analysis-tab',label='Age Analysis', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-5'),
                    dcc.Tab(id='risk-tab',label='At Risk Groups', selected_style={
                            'border-top': '2px solid #2A3F5F'}, value='tab-6'),
                ]),
                html.Div(id='demographics-tab-div'),

            ])],
            width={"size": 10, "offset": 1}
        )
    ),


],
    style={'marginTop': 100})


@app.callback(Output('demographics-tab-div', 'children'),
              Input('demographics-tabs', 'value'))
def render_content(tab):
    if tab == 'tab-1':
        return html.Div([
            dcc.Graph(figure=race_fig),
            dcc.Graph(figure=race_population_fig)
        ])
    elif tab == 'tab-2':
        return html.Div([
            dcc.Graph(figure=age_fig),
            dcc.Graph(figure=age_population_fig)
        ])
    elif tab == 'tab-3':
        return html.Div([
            dcc.Graph(figure=gender_one_dose_fig),
            dcc.Graph(figure=gender_fully_vaccinated_fig),
            dcc.Graph(figure=gender_population_fig)
        ])
    elif tab == 'tab-4':
        return html.Div([
            dcc.Graph(figure=gender_analysis_fig)
        ])
    elif tab == 'tab-5':
        return html.Div([
            dcc.Graph(figure=age_analysis_fig)
        ])
    elif tab == 'tab-6':
        return html.Div([
            pregnant_women_table,
            dcc.Graph(figure =pregnant_women_age_fig),
            dcc.Graph(figure =pregnant_women_race_fig),
            increased_risk_groups_fig,
            correctional_facilities_table,
        ])

@app.callback(Output('demographics_card', 'style'),
              Input('toggle-theme', 'value'),
              State('demographics_card', 'style'))
def toggle_demographics_theme(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style

