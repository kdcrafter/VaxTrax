import dash
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import plotly.express as px

import pandas as pd
from datetime import datetime

from dash_setup import app
from .data import *

@ app.callback(
    Output('app-1-display-v', 'children'),
    Output('total-table', 'children'),
    Output('perscentage-of-usage-of-manufacturers', 'children'),
    Input('app-1-d', 'value'))
def display_value(value):
    return 'You have selected "{}"'.format(value)

# update_date()
# Tracks all date input componants
# If date input componant is triggered all other inputs are set to that date
# the current selected date is stored in date-output-container
@ app.callback(
    [Output('date-p', component_property='date'),
     Output('date-s', component_property='value'),
     Output('timelapse-i', 'disabled'),
     Output('timelapse-i', 'n_intervals')],
    [Input('date-s', 'value'),
     Input('date-p', 'date'),
     Input('timelapse-i', 'disabled'),
     Input('timelapse-i', 'n_intervals'),
     Input('timelapse-btn-p', 'n_clicks')])
def update_date(value, date, disabled, n_intervals, n_clicks):
    # make date constant across inputs
    if date and "T" in date:
        date = datetime.strptime(
            date, "%Y-%m-%dT00:00:00+00:00").strftime("%m/%d/%Y")
    elif date and "-" in date:
        date = datetime.strptime(date, "%Y-%m-%d").strftime("%m/%d/%Y")

    # Identify triggered input
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

    # handle timelapse
    if 'timelapse-i' in changed_id:

        value = len(dates) - 1 - (dates[len(dates) - 1] -
                                  dates[n_intervals]).days
        date = dates[n_intervals].strftime("%m/%d/%Y")

    # handle slider
    elif 'date-s' in changed_id:
        date = dates[value].strftime("%m/%d/%Y")

    # handle picker
    elif 'date-p' in changed_id:
        if not date:
            raise PreventUpdate
        value = len(dates) - 1 - (datetime.strptime(dates[len(dates) - 1].strftime('%Y-%m-%d'), '%Y-%m-%d') -
                                  datetime.strptime(date, "%m/%d/%Y")).days

    # toggle timelapse
    elif 'timelapse-btn-p' in changed_id:
        if disabled:
            disabled = False
            if n_intervals == len(dates) - 1:
                value = 0
                date = dates[value].strftime("%m/%d/%Y")
        else:
            disabled = True

    return [date, value, disabled, value]


# update_graph()
# updates graphs and header data baised on selected date and state
# feel free to add outputs to other graphs
@ app.callback(
    Output('vaccine-g', 'figure'),
    Output('header-d', 'children'),
    Input('date-p', component_property='date'),
    Input('manufacture-lists', 'value'))
def update_graph(children, value):
    if not value:
        raise PreventUpdate

    data = manufacturers_data.copy()
    data = data[data['date'] == datetime.strptime(children, '%m/%d/%Y').strftime('%Y-%m-%d')]

    world = list(manufacturers_data['location'].unique())

    # selected manufacture
    data = manufacturers_data.copy()
    code_list = []
    index = 0
    for i in world:
        for j in range(index, len(data['vaccine'])):
            code = "z"
            visit = False
            if i == data['location'][j]:
                if j == len(data['vaccine']) - 2 :
                    if visit == False and i != data['location'][j+1]:
                        code_list.append(str(code))
                    else :
                        code_list.append(str("z"))
                elif data['vaccine'][j] == value:
                    code = data['world_abbrev'][j]
                    if data['date'][j] == datetime.strptime(children, '%m/%d/%Y').strftime('%Y-%m-%d') :
                        code = data['world_abbrev'][j]
                        visit = True
                        code_list.append(str(code))
                    else :
                        code_list.append(str("z"))
                else :
                    code_list.append(str("z"))
            else :
                index = j
                break

    manufacturers_data['match'] = code_list
    country_data = manufacturers_data[manufacturers_data.match != "z"]

    if country_data.empty:
        # workaround to show no countires by showing only the smallest country
        fig = px.choropleth(locations=['VCS'])
    else:
        fig = px.choropleth(
            data_frame=country_data,
            locations='world_abbrev',
            color='location',
            hover_data=['location', 'total_vaccinations'],
            labels={'location': 'Country Name', 'total_vaccinations': 'Total Vaccination'},
        )

    fig.update_layout(
        geo = {'bgcolor': 'white'},
        plot_bgcolor = 'white',
        paper_bgcolor = 'white',
        font_color = '#2a3f5f'
    )

    msg = (
        f'{value}\n'
        f'{children}\n'
    )

    return fig, msg

# @app.callback(
#     Output('perscentage-of-usage-of-manufacturers', 'figure'),
#     Input('app-1-d', 'value'),
# )
# def percentage_graph():
#     labels = ['Oxygen','Hydrogen','Carbon_Dioxide','Nitrogen']
#     values = [4500, 2500, 1053, 500]

#     # Use `hole` to create a donut-like pie chart
#     fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
#     return fig

@app.callback(
    Output('world-choropleth-manufacture', 'figure'), 
    Input('select-manufacture', 'value'),
)
def display_choropleth(manufacture):
    if not manufacture:
        raise PreventUpdate

    data = world_code.copy()
    code_list = []
    for i in range(len(data['vaccines'])):
        vaccine_split = str(data['vaccines'][i]).split(", ")
        visit = False
        for j in range(len(vaccine_split)):
            if (vaccine_split[j]) == manufacture:
                code_list.append(str(data['iso_code'][i]))
                visit = True
        if visit == False :
            code_list.append(str("z"))

    data['match'] = code_list

    country_data = data[data.match != "z"]
    fig = px.choropleth(
        data_frame=country_data,
        locations='iso_code',
        color='location',
        hover_data=['location'],
        labels={'location': 'Country Name', 'iso_code' : 'ISO-Code'}
    )

    fig.update_layout(
        plot_bgcolor = 'white',
        paper_bgcolor = 'white',
        font_color = '#2a3f5f'
    )
    return fig

@app.callback(
    Output('daily-total', 'figure'),
    Input('manufacture-lists', 'value')
)
def update_charts(manufacture):
    if not manufacture:
        raise PreventUpdate
    df_state_graph = manufacturers_data.loc[manufacturers_data['vaccine'] == manufacture]
    df_state_graph['date'] = pd.to_datetime(
        df_state_graph['date'], format='%Y-%m-%d')
    graph = px.scatter(df_state_graph, 'date', 'total_vaccinations',
                        color='location'
                       )

    return graph

@app.callback(Output('manufacturer-card-1', 'style'),
              Input('toggle-theme', 'value'),
              State('manufacturer-card-1', 'style'))
def toggle_manufacturer1_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style

@app.callback(Output('manufacturer-card-2', 'style'),
              Input('toggle-theme', 'value'),
              State('manufacturer-card-2', 'style'))
def toggle_manufacturer2_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style

@app.callback(Output('manufacturer-card-3', 'style'),
              Input('toggle-theme', 'value'),
              State('manufacturer-card-3', 'style'))
def toggle_trend_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style