import pandas as pd
from datetime import datetime
import datetime as DT
import matplotlib.dates as mdates

from sources import manufacturers_data, world_code

#split manufactures into list
manufacture_list = []
for i in range(len(world_code['vaccines'])):
    vaccine_split = str(world_code['vaccines'][i]).split(", ")
    for j in range(len(vaccine_split)):
        if str(vaccine_split[j]) not in manufacture_list:
            manufacture_list.append(str(vaccine_split[j]))

manufacture_list.sort(reverse=False)
    
#format world
world = list(manufacturers_data['location'].unique())
manufactures = list(manufacturers_data['vaccine'].unique())

#format dates from data
dates = list(manufacturers_data['date'].unique())

# convert to date types
dates = mdates.num2date(mdates.drange(datetime.strptime(dates[23], '%Y-%m-%d'),
                                      datetime.strptime(
    dates[len(dates) - 1], '%Y-%m-%d') + DT.timedelta(days=1),
    DT.timedelta(days=1)))

dates.sort(reverse=True)
manufactures.sort()


total_by_manufactures = []
for i in manufactures:
    total = 0
    sum = 0
    for j in world:
        for k in range(len(manufacturers_data['location'])):
            if j == manufacturers_data['location'][k]:
                if i == manufacturers_data['vaccine'][k]:
                    total = manufacturers_data['total_vaccinations'][k]
        sum += total
    total_by_manufactures.append(sum)