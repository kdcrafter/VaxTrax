import pandas as pd
import plotly.figure_factory as ff
import plotly.express as px

df = pd.read_csv("https://raw.githubusercontent.com/tonmcg/US_County_Level_Election_Results_08-20/master/2020_US_County_Level_Presidential_Results.csv")
df_electoral = pd.read_csv("2020ElectoralCollege.csv")

def findWinnerElectoral(row):
    if row['ELECTORAL VOTES CAST FOR JOSEPH R. BIDEN (D)'] > row['ELECTORAL VOTES CAST FOR DONALD J. TRUMP (R)']:
        return 'Democrat'
    if row['ELECTORAL VOTES CAST FOR JOSEPH R. BIDEN (D)'] < row['ELECTORAL VOTES CAST FOR DONALD J. TRUMP (R)']:
        return 'Republican'

def findWinnerCounty(row):
    if row['votes_gop'] > row['votes_dem']:
        return 'GOP'
    if row['votes_gop'] < row['votes_dem']:
        return "DEM"

df['winner'] = df.apply(lambda row: findWinnerCounty(row), axis=1)
df_electoral['winner'] = df_electoral.apply(lambda row: findWinnerElectoral(row), axis=1)

values = df['winner'].tolist()
fips = df['county_fips'].tolist()
colorscale = ['rgb(50, 50, 168)', 'rgb(168, 50, 50)']

fig = ff.create_choropleth(
    fips=fips,
    values=values,
    scope=['usa'],
    round_legend_values=True,
    simplify_county=0,
    simplify_state=0,
    legend_title='GOP numbers per county',
    colorscale=colorscale
)

fig.show()

fig2 = px.choropleth(data_frame=df_electoral,
                     locations=df_electoral['STATE'],
                     locationmode="USA-states",
                     color_continuous_scale=['rgb(168, 50, 50)', 'rgb(50, 50, 168)'],
                     color=df_electoral['winner'],
                     scope="usa")

fig2.update_geos(fitbounds="locations", visible=True)

fig2.show()