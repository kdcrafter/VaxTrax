import dash
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from datetime import datetime
import datetime as DT
import time

import plotly.graph_objects as go

from dash_setup import app
from .data import *
from util import state_covid_link, state_names, state_vaccination_eligibility, state_codes, graph_themes
from sources import state_cases

# Vaccine efficiency


@ app.callback(
    Output('app-1-display-value', 'children'),
    Input('app-1-dropdown', 'value'))
def display_value(value):
    return 'You have selected "{}"'.format(value)


# update_date()
# Tracks all date input componants
# If date input componant is triggered all other inputs are set to that date
# the current selected date is stored in date-output-container
@ app.callback(
    [Output('date-output-container', 'children'),
     Output('date-picker', component_property='date'),
     Output('date-slider', component_property='value'),
     Output('timelapse-interval', 'disabled'),
     Output('timelapse-interval', 'n_intervals'),
     Output('timelapse-btn-play', 'children')],
    [Input('date-slider', 'value'),
     Input('date-picker', 'date'),
     Input('timelapse-interval', 'disabled'),
     Input('timelapse-interval', 'n_intervals'),
     Input('timelapse-btn-play', 'n_clicks'),
     Input('timelapse-btn-play', 'children')])
def update_date(value, date, disabled, n_intervals, n_clicks, buttonLabel):
    # make date constant across inputs
    if date and "T" in date:
        date = datetime.strptime(
            date, "%Y-%m-%dT00:00:00+00:00").strftime("%m/%d/%Y")
    elif date and "-" in date:
        date = datetime.strptime(date, "%Y-%m-%d").strftime("%m/%d/%Y")

    # Identify triggered input
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

    # handle timelapse
    if 'timelapse-interval' in changed_id:

        value = len(datetimes) - 1 - \
            (datetimes[-1] - datetimes[n_intervals]).days
        date = datetimes[n_intervals].strftime("%m/%d/%Y")

        if n_intervals == len(datetimes) - 1:
            buttonLabel = "Play"
            disabled = True

    # handle slider
    elif 'date-slider' in changed_id:
        date = datetimes[value].strftime("%m/%d/%Y")

    # handle picker
    elif 'date-picker' in changed_id:
        if not date:
            raise PreventUpdate
        value = len(datetimes) - 1 - (datetime.strptime(datetimes[-1].strftime('%Y-%m-%d'), '%Y-%m-%d') -
                                      datetime.strptime(date, "%m/%d/%Y")).days

    # toggle timelapse
    elif 'timelapse-btn-play' in changed_id:
        if disabled:
            disabled = False
            buttonLabel = "Pause"
            if n_intervals == len(datetimes) - 1:
                value = 0
                date = datetimes[value].strftime("%m/%d/%Y")
        else:
            buttonLabel = "Play"
            disabled = True

    return ["{}".format(date), date, value, disabled, value, buttonLabel]


# update_graph()
# updates graphs and header data baised on selected date and state
# feel free to add outputs to other graphs
@ app.callback(
    Output('main-graph', 'figure'),
    Output('graph-title', 'children'),
    Output('state-output-container', 'children'),
    Output('data-1', 'children'),
    Output('data-2', 'children'),
    Output('data-3', 'children'),
    Output('data-4', 'children'),
    Output('estimated', 'children'),
    Output('estimatedLink', 'href'),
    Output('vaccine_eligible', 'children'),
    Output('usmap-tooltip', 'children'),
    Input('date-output-container', 'children'),
    Input('state-dropdown', 'value'),
    Input('graph-theme', 'value'),
    Input('main-tabs', 'value'))
def update_graph(children, value, graphTheme, tab):
    if not value or not graphTheme:
        raise PreventUpdate

    data = state_vaccs.copy()
    data = data[data['date'] == datetime.strptime(
        children, '%m/%d/%Y').strftime('%Y-%m-%d')]

    state = value

    currTab = 0
    tooltipMessage = ["The percentage of available vaccines that have been used. A low Administered Efficiency may be an indication that vaccines are being wasted. Calculated by Administered / Distributed * 100",
                      "The total vaccinations administered. This includes both first & second doses. As expected the data closely matches population distribution.",
                      "The percentage of cumulative counts of the COVID-19 vaccine doses recorded as shipped relative to the population.",
                      "The number of vaccines administered for the selected date. "]

    data_modified = data[data.state_abbrev != "US"]

    title = ""

    if tab == 'tab-1':
        fig = go.Figure(data=go.Choropleth(
            locations=data['state_abbrev'],
            z=round(data['vaccine_ratio'].astype(float), 2),
            locationmode='USA-states',
            colorscale=graph_themes[graphTheme],
            text=data['state'],  # hover text
            marker_line_color='#FFFFFF',
            colorbar_title="Administered Efficiency",
        ))
        title = "COVID-19 Administered Efficiency (Administered/Distributed)"
        currTab = 0
    elif tab == 'tab-2':
        fig = go.Figure(data=go.Choropleth(
            locations=data_modified['state_abbrev'],
            z=round(data_modified['people_vaccinated'].astype(float), 2),
            locationmode='USA-states',
            colorscale=graph_themes[graphTheme],
            text=data_modified['state'],  # hover text
            marker_line_color='#FFFFFF',
            # title='<b>COVID-19 Daily Administered Vaccines in US</b>',
            colorbar_title="Total Vaccinations",
        ))
        title = "COVID-19 Total Vaccinations"
        currTab = 1
    elif tab == 'tab-3':
        fig = go.Figure(data=go.Choropleth(
            locations=data_modified['state_abbrev'],
            z=round(data_modified['distributed_per_hundred'].astype(float), 2),
            locationmode='USA-states',
            colorscale=graph_themes[graphTheme],
            text=data_modified['state'],  # hover text
            marker_line_color='#FFFFFF',
            # title='<b>COVID-19 Daily Distributed Vaccines in US</b>',
            colorbar_title="Distributed by Population",
        ))
        title = "COVID-19 Distributed Vaccines Per Hundred"
        currTab = 2
    elif tab == 'tab-4':
        fig = go.Figure(data=go.Choropleth(
            locations=data_modified['state_abbrev'],
            z=round(data_modified['daily_vaccinations'].astype(float), 2),
            locationmode='USA-states',
            colorscale=graph_themes[graphTheme],
            text=data_modified['state'],  # hover text
            marker_line_color='#FFFFFF',
            # title='<b>COVID-19 Daily Administered Vaccines in US</b>',
            colorbar_title="Administered",
        ))
        title = "COVID-19 Daily Administered Vaccines"
        currTab = 3

    fig.update_layout(
        margin={'t': 0, 'b': 0, 'l': 0},
        geo_scope='usa',
        geo={'bgcolor': 'white'},
        plot_bgcolor='white',
        paper_bgcolor='white',
        font_color='#2a3f5f',
        geo_lakecolor='#FFFFFF',
    )

    curr_data = data[data['state'] == state]
    # print(curr_data)
    state_label = state

    # Populate Header Data

    efficiency = curr_data['vaccine_ratio'].unique()
    if efficiency >= 0:
        efficiency = "Efficiency: " + ("%.2f" % (efficiency[0] * 100)) + '%'
    else:
        efficiency = "Efficiency: N/A"
    peopleVac = curr_data['people_vaccinated'].unique()
    if peopleVac >= 0:
        peopleVac = "Total Vaccinated: " + ("%.0f" % peopleVac[0])
    else:
        peopleVac = "Total Vaccinated: N/A"
    admin = curr_data['daily_vaccinations'].unique()
    if admin >= 0:
        admin = "Daily Administered: " + ("%.0f" % admin[0])
    else:
        admin = "Daily Administered: N/A"
    dist = curr_data['distributed_per_hundred'].unique()
    if dist >= 0:
        dist = "Distributed Per Hundred: " + ("%.2f" % dist[0])
    else:
        dist = "Distributed Per Hundred: N/A"

    if state in state_codes:
        vacc_eligible = state + " Vaccine Eligibility: " + \
            state_vaccination_eligibility[state_codes[state]]
    else:
        vacc_eligible = ""
    
    if  curr_data['estimated'].any():
        estimatedMessage = "*Values were estimated using linear interpolation"
        estimatedLink= "https://en.wikipedia.org/wiki/Linear_interpolation"
        title = '*' + title
    else :
        estimatedMessage = ''
        estimatedLink = ''

    return fig, title, state_label, admin, dist, efficiency, peopleVac,  estimatedMessage, estimatedLink, vacc_eligible, tooltipMessage[currTab]

# State vaccinations


@ app.callback(Output('state-fig', 'figure'),
               Output('state-title', 'children'),
               Output('state-population', 'children'),
               Output('state-fully-vaccinated', 'children'),
               Output('state-percent-vaccinated', 'children'),
               Output('state-daily-vaccinations', 'children'),
               Output('state-vaccination-data', 'figure'),
               Output('state-covid', 'href'),
               Input('total-people-vaccinated', 'clickData'),
               Input('state-y-axis', 'value'),
               Input('state-trendline', 'value'))
def display_state_percentage(clickData, statey, trendType):
    if not statey:
        raise PreventUpdate

    try:
        state = clickData['points'][0]['location']
    except TypeError:  # Catches error when callback is called on init
        state = 'IN'
    df_state = df_latest.copy()
    df_state = df_state.loc[df_state['state_abbrev'] == state]
    state_fully_vaccinated = df_state['people_fully_vaccinated'].values[0]
    state_fig = px.choropleth(data_frame=df_state,
                              locations=[state],
                              locationmode="USA-states",
                              color=df_state['people_fully_vaccinated'],
                              scope="usa",
                              labels={'people_fully_vaccinated': 'People Fully Vaccinated'})
    state_fig.update_geos(fitbounds="locations", visible=True)

    state_pop_row = state_pop.loc[state_pop['state_abbrev'] == state]
    base = state_pop_row['pop_july_2019'].values[0]
    daily_growth = state_pop_row['daily_growth'].values[0]
    curr_state_pop = estimate_pop_on_date(
        base, daily_growth, pd.to_datetime(dates[-1]))
    percent_vaccinated = round(
        (state_fully_vaccinated/curr_state_pop) * 100, 2)

    state_title = state_names[state]
    state_population = "State population: " + '{:,}'.format(curr_state_pop)
    state_fully_vaccinated = "State fully vaccinated: " + \
        '{:,}'.format(state_fully_vaccinated)
    state_percent_vaccinated = "Percent fully vaccinated: " + \
        '{:,}'.format(percent_vaccinated) + "%"
    state_daily_vaccinations = "Daily Vaccinations: " + \
        '{:,}'.format(df_state['daily_vaccinations'].values[0])

    df_state_graph = state_vaccs.loc[state_vaccs['state_abbrev'] == state]
    df_state_graph['date'] = pd.to_datetime(
        df_state_graph['date'], format='%Y-%m-%d')
    graph = px.scatter(df_state_graph, 'date', statey,
                       title="Vaccination Data for " + state_title, trendline=trendType,
                       labels={statey: statey.replace("_", " ").title(), 'date': 'Date'})
    state_link = state_covid_link[state]
    return state_fig, state_title, state_population, state_fully_vaccinated, state_percent_vaccinated, state_daily_vaccinations, graph, state_link

# Herd immunity


@app.callback(
    Output('immunity-progress1', 'value'),
    Output('immunity-progress2', 'value'),
    Output('immunity-progress1', 'color'),
    Output('immunity-progress2', 'color'),
    Output('immunity-text1', 'children'),
    Output('immunity-text2', 'children'),
    Input('total-people-vaccinated', 'clickData'))
def update_immunity_progress(clickData):
    try:
        state = clickData['points'][0]['location']
    except TypeError:  # Catches error when callback is called on init
        state = 'IN'

    date = dates[-1]
    curr_state_cases = state_cases[(state_cases['state_abbrev'] == state) & (
        state_cases['date'] == date)]['tot_cases'].iloc[0]
    state_vaccinations = state_vaccs[(state_vaccs['state_abbrev'] == state) & (
        state_vaccs['date'] == date)]['people_vaccinated'].iloc[0]

    state_pop_row = state_pop.loc[state_pop['state_abbrev'] == state]
    base = state_pop_row['pop_july_2019'].values[0]
    daily_growth = state_pop_row['daily_growth'].values[0]
    curr_state_population = estimate_pop_on_date(
        base, daily_growth, pd.to_datetime(date))

    percent_immune = 100 * \
        (curr_state_cases + state_vaccinations) / curr_state_population
    percent_immune = min(percent_immune, 100.0)
    remaining_immune = THRESHOLD - percent_immune

    if percent_immune < THRESHOLD:
        return (percent_immune, remaining_immune, BELOW_THRESHOLD_COLOR, REMAINING_THRESHOLD_COLOR,
                below_threshold_text(percent_immune), remaining_threshold_text(remaining_immune))
    else:
        return (percent_immune, 0, ABOVE_THRESHOLD_COLOR, ABOVE_THRESHOLD_COLOR,
                below_threshold_text(percent_immune), ABOVE_THRESHOLD_TEXT)

# Vaccine rankings


@app.callback(
    Output('vaccine-ranking-graph', 'figure'),
    Output('vaccine-ranking-order-button', 'children'),
    Output('vaccine-ranking-tooltip', 'children'),
    Input('vaccine-ranking-radio', 'value'),
    Input('vaccine-ranking-order-button', 'n_clicks'))
def update_vaccine_ranking_fig(value, n_clicks):
    df = state_only_vaccs[state_only_vaccs['date'] == dates[-1]]

    if value == None or value == 'VE':
        fig = px.bar(df, x='state_abbrev', y='vaccine_ratio', title='State Vaccine Efficiency Ranking',
                     labels={'state_abbrev': 'State', 'vaccine_ratio': 'Vaccine Efficiency'})

        message = 'The percentage of available vaccines that have been used ranked by each state. \
                   Calculated by the number of vaccines administered divided by the number of \
                   vaccines distributed.'
    else:
        fig = go.Figure(data=[
            go.Bar(name='Percent Single-Dose Vaccinated',
                   x=df['state_abbrev'], y=df['people_partly_vaccinated_per_hundred']/100),
            go.Bar(name='Percent Fully Vaccinated',
                   x=df['state_abbrev'], y=df['people_fully_vaccinated_per_hundred']/100)
        ], layout=go.Layout(title='State Percent Vaccinated Ranking', xaxis_title='State', yaxis_title='Percent Vaccinated'))

        fig.update_layout(barmode='stack')
        fig.update_layout(legend=dict(
            orientation='h', yanchor="bottom", y=1.02, xanchor="right", x=1))

        message = 'The percentage of people vaccinated with one or two doses ranked by each state. \
                   Calculated by the number of people vaccined with one dose + people vaccined with \
                   two doses all divided by the population.'

    if n_clicks == None or n_clicks % 2 == 0:
        icon = '\U00002191'  # up arrow
        fig.update_layout(xaxis={'categoryorder': 'total ascending'})
    else:
        icon = '\U00002193'  # down arrow
        fig.update_layout(xaxis={'categoryorder': 'total descending'})

    return fig, icon, message


@app.callback(Output('main-card', 'style'),
              Input('toggle-theme', 'value'),
              State('main-card', 'style'))
def toggle_main_card_theme(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style


@app.callback(Output('state-vaccs-card', 'style'),
              Input('toggle-theme', 'value'),
              State('state-vaccs-card', 'style'))
def toggle_state_vacc_theme(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style


@app.callback(Output('vacc-rank-card', 'style'),
              Input('toggle-theme', 'value'),
              State('vacc-rank-card', 'style'))
def toggle_vacc_rank_theme(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style
