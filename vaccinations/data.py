import pandas as pd
from datetime import datetime
import datetime as DT
import matplotlib.dates as mdates
import time

from util import state_codes, estimate_pop_on_date
from sources import state_cases, state_vaccs, state_pop, manufacturers_data

# Vaccinations
vacc_cols = ['date', 'state', 'state_abbrev', 'total_vaccinations', 'total_distributed', 'people_vaccinated',
             'distributed_per_hundred', 'daily_vaccinations', 'people_fully_vaccinated',
             'people_vaccinated_per_hundred', 'people_fully_vaccinated_per_hundred', 'estimated']
usable_vacc_cols = vacc_cols[3:]
state_vaccs['vaccine_ratio'] = state_vaccs['total_vaccinations'] / \
    state_vaccs['total_distributed']
state_vaccs['people_partly_vaccinated_per_hundred'] = state_vaccs['people_vaccinated_per_hundred'] - \
    state_vaccs['people_fully_vaccinated_per_hundred']

state_only_vaccs = state_vaccs[state_vaccs['state'].isin(state_codes.keys())]  # remove US

states = list(state_vaccs['state'].unique())
df_latest = state_vaccs[state_vaccs.date == state_vaccs.date.max()]
date_earliest = state_vaccs.date.min()
dates1 = set(state_vaccs['date'])
dates2 = set(state_cases['date'])
dates = sorted(list(dates1.intersection(dates2)))

# convert to date types
datetimes = mdates.num2date(mdates.drange(
    datetime.strptime(dates[0], '%Y-%m-%d'),
    datetime.strptime(dates[-1], '%Y-%m-%d') + DT.timedelta(days=1),
    DT.timedelta(days=1))
)

datetimes.sort(reverse=True)

# Population
state = 'IN'
df_state = df_latest.copy()
df_state = df_state.loc[df_state['state_abbrev'] == state]

state_pop_row = state_pop.loc[state_pop['state_abbrev'] == state]
base = state_pop_row['pop_july_2019'].values[0]
daily_growth = state_pop_row['daily_growth'].values[0]
curr_state_pop = estimate_pop_on_date(base, daily_growth, pd.to_datetime(dates[-1]))

# Herd Immunity
THRESHOLD = 70.0  # percent

BELOW_THRESHOLD_COLOR = 'orange'
REMAINING_THRESHOLD_COLOR = '#ffd78c'  # light orange
ABOVE_THRESHOLD_COLOR = 'green'

def below_threshold_text(x):
    return f'An estimated {round(x, 2)}% is immune'

def remaining_threshold_text(x):
    return f'{round(x, 2)}% more until herd immunity is acheived'

ABOVE_THRESHOLD_TEXT = 'Herd immunity is acheived'

# Manufacturers data
world = list(manufacturers_data['location'].unique())
manufactures = list(manufacturers_data['vaccine'].unique())
date_list = list(manufacturers_data['date'].unique())
date_list = mdates.num2date(mdates.drange(datetime.strptime(date_list[23], '%Y-%m-%d'),
                                          datetime.strptime(
                                              date_list[len(date_list) - 1], '%Y-%m-%d') + DT.timedelta(days=1),
                                          DT.timedelta(days=1)))
date_list.sort(reverse=True)
manufactures.sort()
