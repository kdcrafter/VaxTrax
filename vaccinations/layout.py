import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table as dt
import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
import pandas as pd
import numpy as np

from dash_setup import app
from . import callbacks
from .data import *

# Total vaccinations given in US
total_today_fig = go.Figure(data=go.Choropleth(
    locations=df_latest['state_abbrev'],  # Spatial coordinates
    z=df_latest['total_vaccinations'],  # Data to be color-coded
    locationmode='USA-states',  # set of locations match entries in `locations`
    colorscale='Reds',
    autocolorscale=False,
    marker_line_color='white',
    colorbar_title="Population",
))

total_today_fig.update_layout(
    title_text='2021 US Total Vaccination for Today(Last Updated): ' +
    dates[-1],
    geo_scope='usa',  # limit map scope to USA
)


# Newly distributed vaccines per day in US
daily_distributed_fig = px.choropleth(state_vaccs,
                                      locations="state_abbrev",
                                      featureidkey="distributed_per_hundred",
                                      color="distributed_per_hundred",
                                      color_continuous_scale="Reds",
                                      animation_frame="date",
                                      range_color=(
                                          0, max(state_vaccs['distributed_per_hundred'])),
                                      scope='usa',
                                      title='<b>COVID-19 daily distributed vaccines in US</b>',

                                      hover_name='distributed_per_hundred',
                                      hover_data={
                                          'distributed_per_hundred': True,
                                          'date': False
                                      },
                                      locationmode='USA-states'
                                      )

# adjust map layout styling
daily_distributed_fig.update_layout(
    showlegend=True,
    margin=dict(l=20, r=0, b=0, t=70, pad=0),
    legend=dict(orientation='v'),
)


# Newly administered vaccines per day in US
# daily_vaccinations data does not exist on the earliest date
df = state_vaccs[state_vaccs['date'] != date_earliest]
daily_administered_fig = px.choropleth(df,
                                       locations="state_abbrev",
                                       featureidkey="daily_vaccinations",
                                       color="daily_vaccinations",
                                       color_continuous_scale="Reds",
                                       animation_frame="date",
                                       range_color=(
                                           0, max(state_vaccs['daily_vaccinations'])),
                                       scope='usa',
                                       title='<b>COVID-19 Daily Administered Vaccines in US</b>',
                                       hover_name='daily_vaccinations',
                                       hover_data={
                                           'daily_vaccinations': True,
                                           'date': False
                                       },
                                       locationmode='USA-states'
                                       )

# adjust map layout styling
daily_administered_fig.update_layout(
    showlegend=True,
    margin=dict(l=20, r=0, b=0, t=70, pad=0),
    legend=dict(orientation='v'),
)

# State vaccinations
fig = go.Figure(data=go.Choropleth(
    locations=df_latest['state_abbrev'],  # Spatial coordinates
    z=df_latest['people_fully_vaccinated'].astype(
        float),  # Data to be color-coded
    locationmode='USA-states',  # set of locations match entries in `locations`
    colorscale=[[0, 'rgb(220,50,50)'], [0.5, 'rgb(250, 180, 0)'], [
        1, 'rgb(0,220,0)']],
    autocolorscale=False,
    marker_line_color='white',
    colorbar_title="People Fully Vaccinated",
))

fig.update_layout(
    title_text='People Fully Vaccinated Today (Last Updated): ' +
    dates[-1],
    geo_scope='usa',  # limite map scope to USA
)

state_fig = px.choropleth(data_frame=df_state,
                          locations=[state],
                          locationmode="USA-states",
                          color_continuous_scale=[[0, 'rgb(220,50,50)'], [0.5, 'rgb(250, 180, 0)'], [
                              1, 'rgb(0,220,0)']],
                          color=df_state['people_fully_vaccinated'],
                          scope="usa",)
state_fig.update_geos(fitbounds="locations", visible=True)


vaccinations_layout = html.Div([
    dbc.Row(
        dbc.Col(
            children=[datetimes.sort(reverse=False),


                      dcc.Interval(
                id='timelapse-interval',
                interval=0.5*1000,
                max_intervals=len(datetimes)-1,
                n_intervals=len(datetimes)-1,
                disabled=True
            ),

                # General Vaccine Graphs
                dbc.Card(id='main-card', body=True, style={'margin-top': '4vh'}, children=[
                    dbc.Row(justify='center', children=[
                            html.H2(id='graph-title'),
                            html.Img(src=app.get_asset_url(
                                'infoicon.png'), height="25px", id="us-map-tooltip-msg",
                                style={'margin-left': '10px'}),
                            dbc.Tooltip(
                                target="us-map-tooltip-msg",
                                placement="auto",
                                style={'width': '100%',
                                       'text-align': 'left', 'padding': '10px'},
                                id='usmap-tooltip'),
                            ]),

                    dbc.Row([
                            dbc.Col(width=2, children=dcc.Tabs(id='main-tabs', value='tab-1', children=[
                                dcc.Tab(id='efficiency-tab', label='Administered Efficiency', selected_style={'border-left': '2px solid #2A3F5F'},
                                        value='tab-1'),
                                dcc.Tab(id='total-vaccs-tab', label='Total Vaccinations', selected_style={'border-left': '2px solid #2A3F5F'},
                                        value='tab-2'),
                                dcc.Tab(id='distributed-tab', label='Distributed By Population', selected_style={'border-left': '2px solid #2A3F5F'},
                                        value='tab-3'),
                                dcc.Tab(id='administered-tab', label='Daily Administered', selected_style={'border-left': '2px solid #2A3F5F'},
                                        value='tab-4'),
                            ], vertical=True),
                            ),
                            dbc.Col(width=7, children=dcc.Graph(id='main-graph', style={'height': '38vh'}),
                                    ),
                            dbc.Col(width=3, children=[
                                html.Div([
                                    html.H3(id='state-output-container'),
                                ], style={'display': 'inline-block'}),
                                html.Div([
                                    html.H3(id='date-output-container'),
                                ], style={'display': 'inline-block'}),

                                html.H5(id='data-1'),
                                html.H5(id='data-2'),
                                html.H5(id='data-3'),
                                html.H5(id='data-4'),
                            ]),
                            ]),
                    dcc.Dropdown(
                        id='state-dropdown',
                        options=[
                            {'label': i, 'value': i} for i in states
                        ],
                        searchable=True,
                        style={'margin-top': 10},
                        value='United States',
                    ),
                    dbc.Row(style={'margin-top': '4vh'}, children=[
                            dbc.Col(width=2, className='center-col', children=[
                                dbc.Button(
                                    'Play', id='timelapse-btn-play', n_clicks=0),
                                html.Img(src=app.get_asset_url('infoicon.png'),
                                         height="25px", id="date-tooltip-msg",
                                         style={'margin-left': '10px'}),
                                dbc.Tooltip(
                                    "Use the slider and date selector to view previous dates data. Use the play button to start and pause the time-lapse. All data above will reflect the selected date. Charts below and on other pages will not be effected.",
                                    target="date-tooltip-msg",
                                    placement="auto",
                                    style={'width': '100%', 'text-align': 'left'}),
                            ]),
                            dbc.Col(width=7, children=dcc.Slider(
                                id='date-slider',
                                min=0,
                                max=(datetimes[-1] - datetimes[0]).days,
                                value=len(dates) - 1,
                                marks={
                                    int(i): {'label': datetimes[i].strftime("%m/%d")} for i
                                    in np.round(np.linspace(0, len(datetimes) - 1, 4)).astype(int)
                                },
                            )),
                            dbc.Col(width=3, className='center-col', children=dcc.DatePickerSingle(
                                id='date-picker',
                                min_date_allowed=datetimes[0],
                                max_date_allowed=datetimes[-1],
                                initial_visible_month=datetimes[-1],
                                date=datetimes[-1],
                            )
                            ),
                            ]),
                    dbc.Row(style={'margin-top': '4vh'}, children=[
                            dbc.Col(
                                [html.H6('Data was last updated: {}'.format(
                                datetimes[-1].strftime("%m/%d/%Y"))),
                                html.A(
                                id="estimatedLink",
                                href='',
                                children=[
                                    html.H6(id="estimated")
                                ],),
                                ]
                            ),
                            dbc.Col(html.H6(id='vaccine_eligible')),
                            ])
                ]),

                # State Vaccinations
                dbc.Card(id='state-vaccs-card', body=True, style={'margin-top': '4vh'}, children=[
                    dbc.Row(id='state-select-container', children=[
                            dbc.Col(width=6, children=dcc.Graph(
                                id='total-people-vaccinated',
                                figure=fig
                            )
                            ),
                            dbc.Col(width=6, children=dcc.Graph(
                                id="state-fig",
                                figure=state_fig
                            )
                            ),
                            ]),
                    dbc.Row(children=[
                        dbc.Col(width=3, style={'text-decoration': 'underline'}, children=html.Div(id='state-container', children=[
                            html.A(id='state-covid', href='', children=[
                                html.H1(
                                    id="state-title",
                                    children=[
                                        "Please select a state to see state statistics"]
                                )
                            ]),
                            html.Br(),
                            html.Label(id="state-population", children=""),
                            html.Br(),
                            html.Label(
                                id="state-fully-vaccinated", children=""),
                            html.Br(),
                            html.Label(
                                id="state-percent-vaccinated", children=""),
                            html.Br(),
                            html.Label(id="state-daily-vaccinations", children=""),
                            dbc.RadioItems(
                                id="state-trendline",
                                options=[
                                            {'label': 'Linear', 'value' : 'ols'},
                                            {'label' : 'Non-linear', 'value' : 'lowess'}
                                        ],
                                value='ols'
                            ),
                            html.Img(src=app.get_asset_url('infoicon.png'),
                                    height="25px", id="unique-identifier",
                                    style={'margin-left': '10px'}),
                            dbc.Tooltip(
                                'The linear trend line is calculated via ordinary least square regression while the non-linear \
                                while the non-linear trendline is calculated via local regression. Total vaccinations is the\
                                total number of vaccinations administered whether it is a one shot vaccine or two shot vaccine\
                                People fully vaccinated refers to the total number of people that have completed a one shot\
                                or two shot vaccine.',
                                target="unique-identifier",
                                placement="auto",
                                style={'width': '100%', 'text-align': 'left'}, ),
                        ], style={'display': 'float', 'padding': '12px', 'text-align': 'center'})
                        ),
                        dbc.Col(id='state-vaccs-container', width=9, children=[
                            dcc.Dropdown(
                                id='state-y-axis',
                                options=[
                                    {'label': col.replace("_", " ").title(), 'value': col} for col in usable_vacc_cols
                                ],
                                value='total_vaccinations'
                            ),
                            dcc.Graph(id="state-vaccination-data")
                        ])
                    ]),
                    dbc.Row(style={'margin': '1vw'}, children=[
                            dbc.Col(width=1),
                            dbc.Col(id='herd-immunity-container', width=10, children=[
                                html.Div([
                                    html.H6(id='immunity-text1',
                                            style={'float': 'left'}),
                                    html.H6(id='immunity-text2',
                                            style={'float': 'right'}),
                                ]),
                                html.Div(style={'clear': 'both'}),

                                dbc.Progress(multi=True, children=[
                                    dbc.Progress(
                                        id='immunity-progress1', bar=True),
                                    dbc.Progress(
                                        id='immunity-progress2', bar=True),
                                ]),
                            ]),
                            dbc.Col(width=1, children=[
                                html.Img(
                                    id="immunity-progress-tooltip-img",
                                    src=app.get_asset_url('infoicon.png'),
                                    height="25px",
                                ),
                                dbc.Tooltip(
                                    "Displays the progress towards herd immunity or the percent of a population that is immune. \
                    A person is considered to be immune from COVID-19 if they have either received one dose of the \
                    vaccine or have recovered from COVID-19. The percent immune is estimated by number of people \
                    vaccinated plus the number of people recovered all divided by the population.",
                                    target="immunity-progress-tooltip-img",
                                    placement="auto",
                                    style={'text-align': 'left'}
                                ),
                            ]),
                            ]),
                ]),

                # Vaccine rankings
                dbc.Card(id='vacc-rank-card', body=True, style={'margin-top': '4vh'}, children=[
                    dbc.Row([
                            dbc.Col(width=1),
                            dbc.Col(width=2, children=dbc.RadioItems(
                                id='vaccine-ranking-radio',
                                options=[
                                    {'label': 'Vaccine Efficiency', 'value': 'VE'},
                                    {'label': 'Percent Vaccinated', 'value': 'PV'}
                                ],
                                value='VE',
                                labelStyle={'display': 'block'},
                            )
                            ),
                            dbc.Col(width=1, children=dbc.Button('\U00002191', id='vaccine-ranking-order-button', size='lg')
                                    ),
                            dbc.Col(width=7),
                            dbc.Col(width=1, children=[
                                html.Img(
                                    id="vaccine-ranking-tooltip-img",
                                    src=app.get_asset_url('infoicon.png'),
                                    height="25px",
                                ),
                                dbc.Tooltip(
                                    id='vaccine-ranking-tooltip',
                                    target="vaccine-ranking-tooltip-img",
                                    placement="auto",
                                    style={'text-align': 'left'}
                                ),
                            ])
                            ]),
                    dcc.Graph(id='vaccine-ranking-graph')
                ]), ],
            width={"size": 10, "offset": 1}
        )
    ),


],
    style={'marginTop': 100}
)
