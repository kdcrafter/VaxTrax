import dash_core_components as dcc
import dash_html_components as html
import dash_daq as daq
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State

from dash_setup import app
from vaccinations.layout import vaccinations_layout
from demographics.layout import demographics_layout
from covidStats.layout import covidStats_layout
from citations.layout import citations_layout
from comparisons.layout import state_comparisons_layout
from manufacturers.layout import manufacturers_layout
from util import graph_themes

navbar = dbc.Navbar(
    id="navbar",
    children=[
        html.A(
            # Use row and col to control vertical alignment of logo / brand
            dbc.Row(
                [
                    dbc.Col(html.Img(src=app.get_asset_url(
                        'logo.png'), height="30px",)),

                ],
                align="center",
                no_gutters=True,

            ),
            href="/",
        ),

        # Navigation tabs
        dbc.NavItem(dbc.NavLink('Vaccinations',
                                href='/vaccinations'),
                    style={
                        'list-style': 'none',
        },),
        dbc.NavItem(dbc.NavLink('Comparisons',
                                href='/comparisons'),
                    style={
                        'list-style': 'none',
        },),
        dbc.NavItem(dbc.NavLink('Demographics',
                                href='/demographics'),
                    style={
                        'list-style': 'none',
        },),
        dbc.NavItem(dbc.NavLink('Covid-19 Cases',
                                href='/covidStats'),
                    style={
                        'list-style': 'none',
        },),
        dbc.NavItem(dbc.NavLink('Manufacturers',
                                href='/manufacturers'),
                    style={
                        'list-style': 'none',
        },),
        dbc.NavItem(dbc.NavLink('Citations',
                                href='/citations'),
                    style={
                        'list-style': 'none',
        },),
        daq.ToggleSwitch(
            id='toggle-theme',
            label=['Light', 'Dark'],
            value=False,  # False for light, True for dark
            className='ml-auto',
        ),

        html.Div([
            dcc.Dropdown(
                id='graph-theme',
                options=[
                    {'label': i, 'value': i} for i in graph_themes
                ],
                value='Temperature',
            ),
        ], style={'padding': '12px', 'width': '200px'}),


    ],
    color="white",
    fixed="top",
)

footer = dbc.Navbar(
    id="footer",
    children=[
        html.Span(
            'VaxTrax is a dashboard used to track the progress of COVID-19 Vaccination Data and COVID-19 Cases.'
        )
    ],
    color="white",
    fixed="bottom",
    style={
        "height": "46px",
    }
)

app.layout = html.Div(id='root', children=[
    navbar,
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content',
             style={'margin-top': '80px', 'margin-bottom': '46px'}),
    html.Br(), # extra space to prevent footer overlap
    footer,

    # for the output of callbacks with no output
    html.Div(id='hidden-div', style={'display': 'none'}),

], style={
    'opacity': '0.8',
    'background-color': 'white',
    'color': '#2a3f5f',
    'position': 'absolute',
    'width': '100%',
    'height': '100%',
    'top': '0px',
    'left': '0px',
    'z-index': '1000',
    'overflow-x': 'hidden', # to remove horizontal scroll bar
})

application = app.server


# Callback for navigating between pages
@app.callback(Output('page-content', 'children'),
              Input('url', 'pathname'))
def display_page(pathname):
    if pathname == '/':
        return vaccinations_layout
    elif pathname == '/vaccinations':
        return vaccinations_layout
    elif pathname == '/demographics':
        return demographics_layout
    elif pathname == '/covidStats':
        return covidStats_layout
    elif pathname == '/citations':
        return citations_layout
    elif pathname == '/comparisons':
        return state_comparisons_layout
    elif pathname == '/manufacturers':
        return manufacturers_layout
    else:
        return '404'

@app.callback(Output('root', 'style'),
              Input('toggle-theme', 'value'),
              State('root', 'style'))
def toggle_root_theme(dark, style):
    style['background-color'] = '#000000' if dark else 'white'
    return style

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
