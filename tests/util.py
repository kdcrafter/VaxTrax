import os

def take_element_screenshot(element, directory, filename):
    if not os.path.isdir(directory):
        os.makedirs(directory)
    element.screenshot(directory + filename)

def take_whole_screenshot(dash_duo, directory, filename):
    if not os.path.isdir(directory):
        os.makedirs(directory)
    dash_duo.driver.save_screenshot(directory + filename)