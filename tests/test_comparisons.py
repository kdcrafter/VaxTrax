from selenium.webdriver.common.action_chains import ActionChains
from time import sleep

from dash_setup import app
import application # register callbacks
from .util import take_element_screenshot, take_whole_screenshot
from vaccinations.data import state_vaccs, dates, state

DIR = 'screenshots/comparisons/'

#User Story 11
def test_co001_state_comparison(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/comparisons')
    sleep(3)

    graph = dash_duo.find_element('#comparison-container')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", graph)
    take_element_screenshot(graph, DIR, 'sv003_state_comparison.png')

    assert dash_duo.get_logs() == [], "browser console should contain no errors"

#User Story 11
def test_co001_state_comparisons_data(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/comparisons')
    sleep(3)

    graph = dash_duo.find_element('#state-comparison-data')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", graph)
    take_element_screenshot(graph, DIR, 'sv004_state_comparisons_data.png')

    assert dash_duo.get_logs() == [], "browser console should contain no errors"