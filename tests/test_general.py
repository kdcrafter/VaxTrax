from time import sleep

from dash_setup import app
import application # register callbacks
from .util import *

DIR = 'screenshots/general/'

#User Story 39
def test_g001_theme_toggle(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/')
    sleep(3)

    take_whole_screenshot(dash_duo, DIR, 'g001_theme_toggle_light1.png')

    toggle = dash_duo.find_element('#toggle-theme')
    toggle.click()
    sleep(2)
    take_whole_screenshot(dash_duo, DIR, 'g001_theme_toggle_dark.png')

    toggle.click()
    sleep(2)
    take_whole_screenshot(dash_duo, DIR, 'g001_theme_toggle_light2.png')

    assert dash_duo.get_logs() == [], "browser console should contain no error"
