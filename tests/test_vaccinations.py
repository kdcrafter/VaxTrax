from dash.dependencies import Input, Output
from time import sleep, time
from selenium.webdriver.common.action_chains import ActionChains

from dash_setup import app
import application # register callbacks
from .util import *

from sources import state_vaccs # for herd_immunity test
from vaccinations.data import dates

# Global variables used in callbacks
counts = {}
times = {}

DIR = 'screenshots/vaccinations/'

# User story 33
def test_va001_total_vaccinations(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    tab = dash_duo.find_element('#total-vaccs-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#main-card')
    take_element_screenshot(card, DIR, 'va001_total_vaccinations.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User story 28
def test_va002_daily_distributed(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    tab = dash_duo.find_element('#distributed-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#main-card')
    take_element_screenshot(card, DIR, 'va002_daily_distributed.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User story 9, 23, 24
def test_va003_daily_efficiency_date_picker(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    card = dash_duo.find_element('#main-card')
    take_element_screenshot(card, DIR, 'va003_daily_efficiency_date_picker1.png')

    picker = dash_duo.find_element('#date-picker')
    dash_duo.clear_input(picker)
    ActionChains(dash_duo.driver).move_to_element(picker).click().send_keys('01/17/2021').perform()
    sleep(1)
    take_element_screenshot(card, DIR, 'va003_daily_efficiency_date_picker2.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User story 22
def test_va004_daily_administered(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    tab = dash_duo.find_element('#administered-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#main-card')
    take_element_screenshot(card, DIR, 'va004_daily_administered.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 5
def test_va005_state_vaccinated_ranking(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    button = dash_duo.find_element('#vaccine-ranking-order-button')
    radio = dash_duo.find_element('#vaccine-ranking-radio')
    card = dash_duo.find_element('#vacc-rank-card')
    take_element_screenshot(card, DIR, 'va005_vaccine_efficiency_ascending.png')

    button.click()
    sleep(1)
    take_element_screenshot(card, DIR, 'va005_vaccine_efficiency_descending.png')

    radio.click() # note: clicking twice does not go back to vaccine efficiency
    sleep(1)
    take_element_screenshot(card, DIR, 'va005_percent_vaccinated_descending.png')

    button.click()
    sleep(1)
    take_element_screenshot(card, DIR, 'va005_percent_vaccinated_ascending.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 2
def test_va006_state_vaccination_percentage(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    container = dash_duo.find_element('#state-container')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -80);") # to move navbar away
    take_element_screenshot(container, DIR, 'va006_state_vaccination_percentage.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 17, 27
def test_va007_state_vaccination_data(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    container = dash_duo.find_element('#state-vaccs-container')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -160);") # to move navbar away
    take_element_screenshot(container, DIR, 'va007_state_vaccination_data_total.png')

    graph = dash_duo.find_element('#total-people-vaccinated')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", graph) # scroll to element above
    dash_duo.select_dcc_dropdown('#state-y-axis', value='Daily Vaccinations')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -160);") # to move navbar away
    sleep(1)
    take_element_screenshot(container, DIR, 'va007_state_vaccination_data_daily.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Sory 4
def test_va008_herd_immunity(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    container = dash_duo.find_element('#herd-immunity-container')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -80);") # to move navbar away
    take_element_screenshot(container, DIR, 'va008_herd_immunity_indiana.png')

    graph = dash_duo.find_element('#total-people-vaccinated')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", graph)
    dash_duo.driver.execute_script("window.scrollBy(0 , -80);") # to move navbar away
    state_vaccs.loc[(state_vaccs['state_abbrev'] == 'IL') & 
        (state_vaccs['date'] == dates[-1]), 'people_vaccinated'] *= 3 # to acheive herd immunity
    dash_duo.click_at_coord_fractions(graph, 0.5, 0.5) # click on Illinois
    sleep(1)

    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -80);") # to move navbar away
    take_element_screenshot(container, DIR, 'va008_herd_immunity_illinois_green.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User story 9
def test_va009_efficiency(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    tab = dash_duo.find_element('#efficiency-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#main-card')
    take_element_screenshot(card, DIR, 'va009_efficiency.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 16
def test_va010_select_state(dash_duo):
    dash_duo.start_server(app)
    sleep(3)

    container = dash_duo.find_element('#state-select-container')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", container)
    dash_duo.driver.execute_script("window.scrollBy(0 , -80);") # to move navbar away
    take_element_screenshot(container, DIR, 'va010_select_state_indiana.png')

    graph = dash_duo.find_element('#total-people-vaccinated')
    dash_duo.click_at_coord_fractions(graph, 0.5, 0.5) # click on Illinois
    sleep(1)
    take_element_screenshot(container, DIR, 'va010_select_state_illinois.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"
