from time import sleep

from dash_setup import app
import application # register callbacks
from .util import *

DIR = 'screenshots/citations/'

# User Story 38
def test_c001_citations(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/citations')
    sleep(3)

    take_whole_screenshot(dash_duo, DIR, 'c001_citations.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"