from selenium.webdriver.common.action_chains import ActionChains
from time import sleep

from dash_setup import app
import application # register callbacks
from .util import take_element_screenshot

DIR = 'screenshots/demographics/'

# User Story 25
def test_d001_age_group_vaccinations(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#age-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd001_age_group_vaccinations.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 13
def test_d002_race_group_vaccinations(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#race-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd002_race_group_vaccinations.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 26
def test_d003_gender_one_dose(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#gender-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd003_gender_one_dose_vaccinations.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 26
def test_d004_gender_fully_vaccinated(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#gender-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd004_gender_fully_vaccinated.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"  

# User Story 10
def test_d005_age_analysis(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#age-analysis-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd005_age_analysis.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

def test_d006_gender_analysis(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#gender-analysis-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd006_gender_analysis.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 49
def test_d007_race_population(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#race-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd007_race_population.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

def test_d008_age_population(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#age-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd008_age_population.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

# User Story 50
def test_d009_increased_risk(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#risk-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd009_increased_risk.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

def test_d010_pregnant_stats(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#risk-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd010_pregnant_stats.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

def test_d011_pregnant_age(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#risk-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd011_pregant_age.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"

def test_d012_pregnant_race(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/demographics')
    sleep(3)

    tab = dash_duo.find_element('#risk-tab')
    tab.click()
    sleep(1)
    card = dash_duo.find_element('#demographics_card')
    take_element_screenshot(card, DIR, 'd012_pregnant_race.png')

    logs = dash_duo.get_logs()
    assert logs == [] or logs == None, "browser console should contain no error"
