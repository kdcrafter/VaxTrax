from dash.dependencies import Input, Output
from time import sleep

from dash_setup import app
import application # register callbacks
from .util import *

# Global variables used in callbacks
counts = {}

DIR = 'screenshots/covid_stats/'

# User story 1
def test_cs001_dropdown(dash_duo):
    counts['graph'] = 0

    @app.callback(Output('hidden-div', 'children'),
                  Input('US graph', 'figure'))
    def count_date_picker_updates(figure):
        global counts
        counts['graph'] += 1

    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/covidStats')
    sleep(3)

    take_whole_screenshot(dash_duo, DIR, 'cs001_dropdown1.png')

    dash_duo.select_dcc_dropdown('#y-axis', value='weekly_icu_admissions')
    sleep(2)
    take_whole_screenshot(dash_duo, DIR, 'cs001_dropdown2.png')

    dash_duo.select_dcc_dropdown('#x-axis', value='iso_code')
    dash_duo.select_dcc_dropdown('#y-axis', value='new_tests') # doesn't work for unknown reasons
    sleep(2)
    take_whole_screenshot(dash_duo, DIR, 'cs001_dropdown3.png')

    assert counts['graph'] == 4 # one for each dropdown select + 1

    del counts['graph']

# User Story 16
def test_cs002_select_state(dash_duo):
    dash_duo.start_server(app)
    dash_duo.wait_for_page(dash_duo.server_url + '/covidStats')
    sleep(10)

    dropdown = dash_duo.find_element('#stateSelection')
    dash_duo.driver.execute_script("arguments[0].scrollIntoView();", dropdown)
    take_whole_screenshot(dash_duo, DIR, 'cs002_select_state1.png')

    dash_duo.select_dcc_dropdown('#stateSelection', value='GA')
    sleep(10)
    take_whole_screenshot(dash_duo, DIR, 'cs002_select_state2.png')

    assert dash_duo.get_logs() == [], "browser console should contain no error"