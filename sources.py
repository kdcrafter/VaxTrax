import pandas as pd

from util import LOCAL
from download import *

state_cases = pd.DataFrame()
state_vaccs = pd.DataFrame()
us_covid_data = pd.DataFrame()
manufacturers_data = pd.DataFrame()
world_code = pd.DataFrame()
county_covid_data = pd.DataFrame()
state_pop = pd.DataFrame()
citations = pd.DataFrame()

if LOCAL:
    state_cases = pd.read_csv('data/state_cases.csv')
    state_vaccs = pd.read_csv('data/state_vaccs.csv')
    us_covid_data = pd.read_csv('data/us_covid_data.csv')
    manufacturers_data = pd.read_csv('data/manufacturers_data.csv')
    world_code = pd.read_csv('data/world_code.csv')
    county_covid_data = pd.read_csv('data/county_covid_data.csv')
    state_pop = pd.read_csv('data/state_pop.csv')
    citations = pd.read_csv('data/citations.csv')
else:
    state_cases = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/state_cases.csv')
    state_cases = preprocess_state_cases(state_cases)

    state_vaccs = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/state_vaccs.csv')
    state_vaccs = preprocess_state_vaccs(state_vaccs)

    us_covid_data = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/us_covid_data.csv')
    us_covid_data = preprocess_us_covid_data(us_covid_data)

    manufacturers_data = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/manufacturers_data.csv')
    manufacturers_data= preprocess_manufacturers_data(manufacturers_data)

    world_code = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/world_code.csv')
    world_code= preprocess_world_code(world_code)

    county_covid_data = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/county_covid_data.csv')
    county_covid_data = preprocess_county_covid_data(county_covid_data)

    state_pop = pd.read_excel('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/state_pop.xlsx')
    state_pop = preprocess_state_pop(state_pop)

    citations = pd.read_csv('https://vaxtrax-data.s3.us-east-2.amazonaws.com/covid/citations.csv')