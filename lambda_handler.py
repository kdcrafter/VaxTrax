import urllib3
import boto3
import csv
import datetime
# A script that uploads csvs on an interval to AWS S3. The script will not work locally, it needs to be placed onto a Python 3.7 Environment in AWS Lambda
# To schedule the script, attach an AWS EventBridge/Cloudwatch Event as a trigger for the lambda 

def lambda_handler(event, context):
    # S3 bucket name
    bucket = 'vaxtrax-data'
    
    # Modify url and key depending on what file that needs to be uploaded
    
    # CDC - State COVID-19 Cases
    url='https://data.cdc.gov/api/views/9mfq-cb36/rows.csv?accessType=DOWNLOAD' 
    key='covid/state_cases.csv' 
    upload_file_from_url_to_s3(url, bucket, key)
    
    # OWID - State COVID-19 Vaccination Data
    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/us_state_vaccinations.csv'
    key='covid/state_vaccs.csv'
    upload_file_from_url_to_s3(url, bucket, key)
    
    # OWID - General U.S. COVID-19 Data
    url='https://covid.ourworldindata.org/data/owid-covid-data.csv'
    key="covid/us_covid_data.csv"
    upload_file_from_url_to_s3(url, bucket, key)
    
    # OWID - Vaccinations by Manufacturer
    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations-by-manufacturer.csv'
    key='covid/manufacturers_data.csv'
    upload_file_from_url_to_s3(url, bucket, key)
    
    # OWID - Vaccinations Administered In Each Country
    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/locations.csv'
    key='covid/world_code.csv'
    upload_file_from_url_to_s3(url, bucket, key)
    
    # John Hopkins - General U.S. County COVID-19 Data
    url='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv'
    key='covid/county_covid_data.csv'
    upload_file_from_url_to_s3(url, bucket, key)
    
    # United States Census Bureau - U.S. State 2019 Population Data
    url='https://www2.census.gov/programs-surveys/popest/tables/2010-2019/state/totals/nst-est2019-01.xlsx'
    key='covid/state_pop.xlsx'
    upload_file_from_url_to_s3(url, bucket, key)
    
    
    create_citations()
    
    
    return {
        'status' : '200 - Download and upload succeeded'
    }
    
def upload_file_from_url_to_s3(url, bucket, key):
    s3=boto3.client('s3')
    http=urllib3.PoolManager()
    s3.upload_fileobj(http.request('GET', url, preload_content=False), bucket, key)
    
def create_citations():
    s3 = boto3.resource('s3')
    bucket = s3.Bucket('vaxtrax-data')
    
    # Create citations CSV file
    file = open("/tmp/csv_file.csv", "w") 
    file.write("Source,Description,Link,Date Retrieved or Modified\n")
    
    # CDC - State COVID-19 Cases
    key = "covid/state_cases.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("Centers for Disease Control and Prevention,State COVID-19 Cases,https://data.cdc.gov/api/views/9mfq-cb36/rows.csv?accessType=DOWNLOAD," + timestamp + "\n")
        
    # OWID - State COVID-19 Vaccination Data
    key = "covid/state_vaccs.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("Our World In Data,State COVID-19 Vaccination Data,https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/us_state_vaccinations.csv," + timestamp + "\n")
        
    # OWID - General U.S. COVID-19 Data
    key = "covid/us_covid_data.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("Our World In Data,General U.S. COVID-19 Data,https://covid.ourworldindata.org/data/owid-covid-data.csv," + timestamp + "\n")       

    # OWID - Vaccinations by Manufacturer
    key = "covid/manufacturers_data.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("Our World In Data,Vaccinations by Manufacturer,https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations-by-manufacturer.csv," + timestamp + "\n")       
    
    # OWID - Vaccinations Administered In Each Country
    key = "covid/world_code.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("Our World In Data,Vaccinations Administered In Each Country,https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/locations.csv," + timestamp + "\n")

    # John Hopkins - General U.S. County COVID-19 Data
    key = "covid/county_covid_data.csv"
    for bucket_object in bucket.objects.filter(Prefix=key):
        timestamp = bucket_object.last_modified.strftime("%b %d %Y")
        file.write("John Hopkins University Center for Systems Science and Engineering,General U.S. County COVID-19 Data,https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv," + timestamp + "\n")
        
    # United States Census Bureau - U.S. State 2019 Population Data
    key = "covid/state_pop.xlsx"
    for bucket_object in bucket.objects.filter(Prefix=key):
        file.write("U.S. Census Bureau,U.S. State 2019 Population Data,https://www2.census.gov/programs-surveys/popest/tables/2010-2019/state/totals/nst-est2019-01.xlsx,Jul 1 2019\n")

    # Demographics for Vaccinations (Manual)
    file.write("Centers for Disease Control and Prevention,Demographics (Manual),https://covid.cdc.gov/covid-data-tracker/#vaccination-demographic,Apr 1 2021\n")
    
    # CDC General
    file.write("Centers for Disease Control and Prevention,Guidelines (Manual),https://www.cdc.gov/coronavirus/2019-ncov/index.html,Apr 1 2021\n")

    # Close and upload citations file
    file.close()
    s3.Bucket('vaxtrax-data').upload_file('/tmp/csv_file.csv','covid/citations.csv')
