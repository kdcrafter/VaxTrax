import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.express as px
import dash_table as dt
import pandas as pd
import pickle

from dash_setup import app
from dash.dependencies import Input, Output, State
from sources import citations

card = dbc.Card(id='description_card',style={'margin-top': 10}, children=[
    dbc.Container(
        [

                dbc.Row(
                    [html.H2("Citations"), ],
                    justify="center",

                ),
                html.P(
                    "The VaxTrax project uses COVID-19 data compiled from several different sources. "
                    "Each data source used will be updated every 6 hours, with a timestamp of when it was updated. "
                    "The demographics for vaccinations are manually updated by the developers. "
                    "Visualizations from the dashboard were created by students of Purdue University for CS40700. ",
                    className="lead",
                ),
                html.P(
                    "Created by: James Truong, Esha Bhatti, Kurtis Dressel, Hara Choi, and Naveen Breen."
                ),
                ],
        fluid=True,
    )
],
)

fig = dt.DataTable(
    id='citations_table',
    columns=[{"name": i, "id": i} for i in citations.columns],
    data=citations.to_dict('records'),
    style_as_list_view=True,
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold'
    },
    style_cell={
        'padding': '5px',
        'overflow': 'hidden',
        'textOverflow': 'ellipsis',
        'maxWidth': 0,
        'whiteSpace': 'normal',
        'height': 'auto',
        'textAlign': 'left',
        'boxShadow': '0 0'
    },
)


citations_layout = [
    html.Div([
        dbc.Row(
            dbc.Col(
                children=[card],
                width={"size": 10, "offset": 1}
            )
        ),
        dbc.Row(
            style={'margin-top': '4vh'},
            children=[dbc.Col(
                fig,
                width={"size": 10, "offset": 1}
            )],
        )
    ],
        style={'marginTop': 100})
]


@app.callback(Output('description_card', 'style'),
              Input('toggle-theme', 'value'),
              State('description_card', 'style'))
def toggle_description_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style