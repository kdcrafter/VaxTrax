import pandas as pd

from util import state_codes, state_covid_link
from sources import state_vaccs

"""All these codes are for the demographic data"""
sex_codes = {
    1 : 'Male',
    2 : 'Female'
}

origin_codes = {
    1 : 'Not Hispanic',
    2 : 'Hispanic'
}
race_codes = {
    1 : 'White Only',
    2 : 'African American',
    3 : 'American Indian',
    4 : 'Asian',
    5 : 'Hawaiian or Pacific Islander',
    6 : 'Two or More Races'
}

vacc_cols = ['date', 'state', 'state_abbrev', 'total_vaccinations', 'total_distributed', 'people_vaccinated', 
        'distributed_per_hundred', 'daily_vaccinations', 'people_fully_vaccinated']
usable_vacc_cols = vacc_cols[3:]
state_vaccinations_today = state_vaccs[state_vaccs.date == state_vaccs.date.max()] # df_latest
"""Election Data for county Map"""
dfElectionByCounty = pd.read_csv("https://raw.githubusercontent.com/tonmcg/US_County_Level_Election_Results_08-20/master/2020_US_County_Level_Presidential_Results.csv")
dfElectionByCounty=dfElectionByCounty[dfElectionByCounty['state_name'].isin(state_codes.keys())]
dfElectionByCounty['state_name'] = dfElectionByCounty['state_name'].apply(lambda x : state_codes[x])
"""Election Data for State Affiliation"""
dfElectionByCollege = pd.read_csv("2020ElectoralCollege.csv")
dfElectionByCollege['STATE'] = dfElectionByCollege['STATE'].astype("string")
"""Demographic Data for State Demographics"""
dfDemographics = pd.read_csv("https://www2.census.gov/programs-surveys/popest/tables/2010-2019/state/asrh/sc-est2019-alldata6.csv")
temp_state_codes = state_codes.copy()
temp_state_codes['District of Columbia'] = 'DC'

dfDemographics['State_abbrev'] = dfDemographics['NAME'].apply(lambda x: temp_state_codes[x])
dfDemographics = dfDemographics[dfDemographics.SEX != 0]
dfDemographics['sex_translated'] = dfDemographics['SEX'].apply(lambda x: sex_codes[x])
dfDemographics = dfDemographics[dfDemographics.ORIGIN != 0]
dfDemographics['origin_translated'] = dfDemographics['ORIGIN'].apply(lambda x: origin_codes[x])
dfDemographics = dfDemographics[dfDemographics.RACE != 0]
dfDemographics['race_translated'] = dfDemographics['RACE'].apply(lambda x: race_codes[x])


def findWinnerElectoral(row):
    if row['ELECTORAL VOTES CAST FOR JOSEPH R. BIDEN (D)'] > row['ELECTORAL VOTES CAST FOR DONALD J. TRUMP (R)']:
        return 'Democrat'
    if row['ELECTORAL VOTES CAST FOR JOSEPH R. BIDEN (D)'] < row['ELECTORAL VOTES CAST FOR DONALD J. TRUMP (R)']:
        return 'Republican'

def findWinnerCounty(row):
    if row['votes_gop'] > row['votes_dem']:
        return 'GOP'
    if row['votes_gop'] < row['votes_dem']:
        return "DEM"

dfElectionByCounty['winner'] = dfElectionByCounty.apply(lambda row: findWinnerCounty(row), axis=1)
dfElectionByCollege['winner'] = dfElectionByCollege.apply(lambda row: findWinnerElectoral(row), axis=1)
