import pandas as pd
import plotly.express as px
import plotly.figure_factory as ff

from dash.dependencies import Input, Output, State
from dash_setup import app
from util import graph_themes

from .data import *
from util import state_names, estimate_pop_on_date
from sources import state_pop
from dash.exceptions import PreventUpdate 

@app.callback(Output('state-comparison-data', 'figure'),
              Input('state1-name', 'value'),
              Input('state2-name', 'value'),
              Input('state-comp-axis', 'value'),
              Input('comparison-trendline', 'value'))
def update_comparison_graph(state1, state2, compData, compTrend):
    if not state1 or not state2 or not compData:
        raise PreventUpdate

    styledCompData = compData.replace("_", " ").title()
    df = state_vaccs[(state_vaccs['state_abbrev'] == state1) |
                     (state_vaccs['state_abbrev'] == state2)]
    title = f'{styledCompData} Comparison Between {state_names[state1]} and {state_names[state2]}'
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    comparison_graph = px.scatter(df, 'date', compData, color='state', title=title,
                               labels={compData: styledCompData, 'date': 'Date'}, trendline=compTrend)
    return comparison_graph


@app.callback(Output('state1-map', 'figure'),
              Output('state1-title', 'children'),
              Output('state1-population', 'children'),
              Output('state1-fully-vaccinated', 'children'),
              Output('state1-percent-vaccinated', 'children'),
              Output('state1-daily-vaccinations', 'children'),
              Output('state1-political-affiliation', 'children'),
              Output('state1-covid', 'href'),
              Output('state1-demographics', 'figure'),
              Input('state1-name', 'value'),
              Input('graph-theme', 'value'),
              Input('state1-data-selection', 'value'),
              Input('state1-demographics-selection','value'))
def select_state1(state1_name, graphTheme, stateData, state1Demographics):
    if not state1_name or not graphTheme:
        raise PreventUpdate

    df_state1 = state_vaccinations_today[state_vaccinations_today['state_abbrev'] == state1_name].copy()

    if (stateData == "Election"):
        df_state1Election = dfElectionByCounty.copy()
        df_state1Election = df_state1Election[df_state1Election['state_name']==state1_name]
        values = df_state1Election['winner'].tolist()
        fips = df_state1Election['county_fips'].tolist()
        colorscale = ['rgb(50, 50, 168)', 'rgb(168, 50, 50)']

        state1_fig = ff.create_choropleth(
            fips=fips,
            values=values,
            scope=[state1_name],
            round_legend_values=True,
            simplify_county=0,
            simplify_state=0,
            legend_title='Election Result per County',
            colorscale=colorscale
        )
    else:
        state1_fig = px.choropleth(data_frame=df_state1,
                                   locations=[state1_name],
                                   locationmode="USA-states",
                                   color_continuous_scale=graph_themes[graphTheme],
                                   color=df_state1['people_fully_vaccinated'],
                                   scope="usa", )

    state1_fig.update_geos(fitbounds="locations", visible=True)
    state_fully_vaccinated = df_state1['people_fully_vaccinated'].values[0]
    pop_data = state_pop.loc[state_pop['state_abbrev']==state1_name]
    base = pop_data['pop_july_2019'].values[0]
    daily_growth = pop_data['daily_growth'].values[0]
    curr_state_pop = estimate_pop_on_date(base, daily_growth)
    percent_vaccinated = round((state_fully_vaccinated / curr_state_pop) * 100, 2)

    state1_title = list(state_codes.keys())[list(
        state_codes.values()).index(state1_name)]
    state1_population = "State population: " + '{:,}'.format(curr_state_pop)
    state1_fully_vaccinated = "State fully vaccinated: " + \
        '{:,}'.format(state_fully_vaccinated)
    state1_percent_vaccinated = "Percent fully vaccinated: " + \
        '{:,}'.format(percent_vaccinated) + "%"
    state1_daily_vaccinations = "Daily Vaccinations: " + \
        '{:,}'.format(df_state1['daily_vaccinations'].values[0])
    dfElectionReduced = dfElectionByCollege[dfElectionByCollege['STATE'] == state1_name]
    state1_political_affiliation = "2020 Political Affiliation: " + dfElectionReduced['winner']
    state_link = state_covid_link[state1_name]
    dfState1Demographics = dfDemographics.query("State_abbrev == @state1_name")
    state1_dem_fig = px.pie(dfState1Demographics, values='POPESTIMATE2019', names=state1Demographics)

    return state1_fig, state1_title, state1_population, state1_fully_vaccinated, state1_percent_vaccinated, state1_daily_vaccinations, state1_political_affiliation, state_link, state1_dem_fig


@app.callback(Output('state2-map', 'figure'),
              Output('state2-title', 'children'),
              Output('state2-population', 'children'),
              Output('state2-fully-vaccinated', 'children'),
              Output('state2-percent-vaccinated', 'children'),
              Output('state2-daily-vaccinations', 'children'),
              Output('state2-political-affiliation', 'children'),
              Output('state2-covid', 'href'),
              Output('state2-demographics', 'figure'),
              Input('state2-name', 'value'),
              Input('graph-theme', 'value'),
              Input('state2-data-selection', 'value'),
              Input('state2-demographics-selection','value'))
def select_state2(state2_name, graphTheme, state2Data, state2Demographics):
    if not state2_name or not graphTheme:
        raise PreventUpdate
        
    df_state2 = state_vaccinations_today[state_vaccinations_today['state_abbrev'] == state2_name].copy()

    if (state2Data == "Election"):
        df_state2Election = dfElectionByCounty.copy()
        df_state2Election = df_state2Election[df_state2Election['state_name'] == state2_name]
        values = df_state2Election['winner'].tolist()
        fips = df_state2Election['county_fips'].tolist()
        colorscale = ['rgb(50, 50, 168)', 'rgb(168, 50, 50)']

        state2_fig = ff.create_choropleth(
            fips=fips,
            values=values,
            scope=[state2_name],
            round_legend_values=True,
            simplify_county=0,
            simplify_state=0,
            legend_title='Election Result per County',
            colorscale=colorscale
        )
    else:
        state2_fig = px.choropleth(data_frame=df_state2,
                                   locations=[state2_name],
                                   locationmode="USA-states",
                                   color_continuous_scale=graph_themes[graphTheme],
                                   color=df_state2['people_fully_vaccinated'],
                                   scope="usa", )
    state2_fig.update_geos(fitbounds="locations", visible=True)

    state_fully_vaccinated = df_state2['people_fully_vaccinated'].values[0]
    pop_data = state_pop.loc[state_pop['state_abbrev']==state2_name]
    base = pop_data['pop_july_2019'].values[0]
    daily_growth = pop_data['daily_growth'].values[0]
    curr_state_pop = estimate_pop_on_date(base, daily_growth)

    percent_vaccinated = round((state_fully_vaccinated / curr_state_pop) * 100, 2)

    state2_title = list(state_codes.keys())[list(
        state_codes.values()).index(state2_name)]
    state2_population = "State population: " + '{:,}'.format(curr_state_pop)
    state2_fully_vaccinated = "State fully vaccinated: " + \
        '{:,}'.format(state_fully_vaccinated)
    state2_percent_vaccinated = "Percent fully vaccinated: " + \
        '{:,}'.format(percent_vaccinated) + "%"
    state2_daily_vaccinations = "Daily Vaccinations: " + \
        '{:,}'.format(df_state2['daily_vaccinations'].values[0])
    dfElectionReduced = dfElectionByCollege[dfElectionByCollege['STATE'] == state2_name]
    state2_political_affiliation = "2020 Political Affiliation: " + dfElectionReduced['winner']
    state_link = state_covid_link[state2_name]
    dfState2Demographics = dfDemographics.query("State_abbrev == @state2_name")
    state2_dem_fig = px.pie(dfState2Demographics, values='POPESTIMATE2019', names=state2Demographics)

    return state2_fig, state2_title, state2_population, state2_fully_vaccinated, state2_percent_vaccinated, state2_daily_vaccinations, state2_political_affiliation, state_link, state2_dem_fig


@app.callback(Output('states-card', 'style'),
              Input('toggle-theme', 'value'),
              State('states-card', 'style'))
def toggle_states_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style

@app.callback(Output('trend-card', 'style'),
              Input('toggle-theme', 'value'),
              State('trend-card', 'style'))
def toggle_trend_card(dark, style):
    style['background-color'] = '#111111' if dark else 'white'
    style['color'] = '#777777' if dark else '#2a3f5f'
    return style