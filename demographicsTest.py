import pandas as pd
import plotly.express as px
from util import state_codes

sex_codes = {
    1 : 'Male',
    2 : 'Female'
}

origin_codes = {
    1 : 'Not Hispanic',
    2 : 'Hispanic'
}
race_codes = {
    1 : 'White Only',
    2 : 'African American',
    3 : 'American Indian',
    4 : 'Asian',
    5 : 'Hawaiian or Pacific Islander',
    6 : 'Two or More Races'
}
df = pd.read_csv("https://www2.census.gov/programs-surveys/popest/tables/2010-2019/state/asrh/sc-est2019-alldata6.csv")
temp_state_codes = state_codes.copy()
temp_state_codes['District of Columbia'] = 'DC'

df['State_abbrev'] = df['NAME'].apply(lambda x: temp_state_codes[x])
df = df[df.SEX != 0]
df['sex_translated'] = df['SEX'].apply(lambda x: sex_codes[x])
df = df[df.ORIGIN != 0]
df['origin_translated'] = df['ORIGIN'].apply(lambda x: origin_codes[x])
df = df[df.RACE != 0]
df['race_translated'] = df['RACE'].apply(lambda x: race_codes[x])

df = df.query("State_abbrev == 'AL'")
fig = px.pie(df, values='POPESTIMATE2019', names='sex_translated')
fig.show()