import pandas as pd
import os

from util import state_codes, country_codes

def preprocess_state_cases(df):
    cols = ['submission_date', 'state', 'tot_cases']
    df = df.filter(items=cols)
    df.rename(columns={'submission_date': 'date', 'state': 'state_abbrev'}, inplace=True)
    df = df[df['state_abbrev'].isin(state_codes.values())]
    df['date'] = pd.to_datetime(df['date']).apply(lambda x: x.strftime('%Y-%m-%d'))

    return df

def preprocess_state_vaccs(df):
    df.rename(columns={'location': 'state'}, inplace=True)

    temp_state_codes = state_codes.copy() # add US and New York as "states"
    temp_state_codes['New York State'] = 'NY'
    temp_state_codes['United States'] = 'US'

    df = df[df['state'].isin(temp_state_codes.keys())]
    df['state_abbrev'] = df['state'].apply(lambda x : temp_state_codes[x])

    df['date'] = pd.to_datetime(df['date'])
    min_state_date = df[df['state'] != 'United States'].date.min()
    df.drop(df[(df['state'] == 'United States') & (df['date'] < min_state_date)].index, inplace = True)

    df.loc[df['people_vaccinated'].isnull(), 'estimated'] = "yes"
    # df.loc[df['people_vaccinated'].notnull(), 'estimated'] = False

    df = df.groupby('state').apply(lambda group: group.interpolate())
    df.sort_values('date', ascending=True, ignore_index=True, inplace=True)
    df['date'] = df['date'].apply(lambda x: x.strftime('%Y-%m-%d'))

    return df

def preprocess_us_covid_data(df):
    cols = ['iso_code', 'continent', 'location', 'date', 'icu_patients', 'hosp_patients', 
            'weekly_icu_admissions', 'weekly_hosp_admissions', 'new_tests', 'total_tests', 'positive_rate', 
            'population', 'median_age', 'aged_65_older', 'aged_70_older']
    df = df.filter(items=cols)
    df = df[df['iso_code'] == 'USA']
    df['date'] = pd.to_datetime(df['date']).apply(lambda x: x.strftime('%Y-%m-%d'))

    return df

def preprocess_manufacturers_data(df):
    df['world_abbrev'] = df['location'].apply(lambda x : country_codes[x])
    df['date'] = pd.to_datetime(df['date']).apply(lambda x: x.strftime('%Y-%m-%d'))

    return df

def preprocess_world_code(df):
    df['last_observation_date'] = pd.to_datetime(df['last_observation_date']).apply(lambda x: x.strftime('%Y-%m-%d'))

    return df

def preprocess_county_covid_data(df):
    df = df[df['Province_State'].isin(state_codes.keys())]
    df['Province_State'] = df['Province_State'].apply(lambda x : state_codes[x])
    df = df.dropna(subset=['FIPS'])
    df['County'] = df['Combined_Key'].apply(lambda x : x.split(',')[0])

    return df

def preprocess_state_pop(df):
    INDEX_STATE = 1
    INDEX_2018 = 11
    INDEX_2019 = 12

    state_pop_temp = df.drop(16).reset_index() # remove DC
    df = pd.DataFrame(columns = ['state_abbrev', 'pop_july_2019', 'daily_growth'], index = range(50))

    for i in range(8, 8+50):
        state = state_pop_temp.iloc[i, INDEX_STATE][1:] # remove .
        state_abbrev = state_codes[state]

        population_2018 = int(state_pop_temp.iloc[i, INDEX_2018])
        population_2019 = int(state_pop_temp.iloc[i, INDEX_2019])
        daily_growth = (population_2019 - population_2018) / population_2018 / 365
        df.loc[i-8] = [state_abbrev, population_2019, daily_growth]

    return df

if __name__ == '__main__':
    date_retrieved = pd.Timestamp.now().strftime("%b %d %Y")

    if not os.path.isdir('data'):
        os.makedirs('data')

    citations = pd.DataFrame(columns=['Source', 'Description', 'Link', 'Date Retrieved or Modified'])

    url = 'https://data.cdc.gov/api/views/9mfq-cb36/rows.csv?accessType=DOWNLOAD'
    state_cases = pd.read_csv(url)
    state_cases = preprocess_state_cases(state_cases)
    state_cases.to_csv('data/state_cases.csv', index=False)
    row = {
        'Source': 'Centers for Disease Control and Prevention',
        'Description': 'State COVID-19 Cases',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/us_state_vaccinations.csv'
    state_vaccs = pd.read_csv(url)
    state_vaccs = preprocess_state_vaccs(state_vaccs)
    state_vaccs.to_csv('data/state_vaccs.csv', index=False)
    row = {
        'Source': 'Our World In Data',
        'Description': 'State COVID-19 Vaccination Data',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://covid.ourworldindata.org/data/owid-covid-data.csv'
    us_covid_data = pd.read_csv(url)
    us_covid_data = preprocess_us_covid_data(us_covid_data)
    us_covid_data.to_csv('data/us_covid_data.csv', index=False)
    row = {
        'Source': 'Our World In Data',
        'Description': 'General U.S. COVID-19 Data',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations-by-manufacturer.csv'
    manufacturers_data = pd.read_csv(url)
    manufacturers_data = preprocess_manufacturers_data(manufacturers_data)
    manufacturers_data.to_csv('data/manufacturers_data.csv', index=False)
    row = {
        'Source': 'Our World In Data',
        'Description': 'Vaccinations by Manufacturer',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/locations.csv'
    world_code = pd.read_csv(url)
    world_code = preprocess_world_code(world_code)
    world_code.to_csv('data/world_code.csv', index=False)
    row = {
        'Source': 'Our World In Data',
        'Description': 'Vaccinations Administered In Each Country',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv'
    county_covid_data = pd.read_csv(url)
    county_covid_data = preprocess_county_covid_data(county_covid_data)
    county_covid_data.to_csv('data/county_covid_data.csv', index=False)
    row = {
        'Source': 'John Hopkins University Center for Systems Science and Engineering',
        'Description': 'General U.S. County COVID-19 Data',
        'Link': url,
        'Date Retrieved or Modified': date_retrieved
    }
    citations = citations.append(row, ignore_index=True)

    url='https://www2.census.gov/programs-surveys/popest/tables/2010-2019/state/totals/nst-est2019-01.xlsx'
    state_pop = pd.read_excel(url)
    state_pop = preprocess_state_pop(state_pop)
    state_pop.to_csv('data/state_pop.csv', index=False)
    row = {
        'Source': 'U.S. Census Bureau',
        'Description': 'U.S. State 2019 Population Data',
        'Link': url,
        'Date Retrieved or Modified': 'Jul 1 2019'
    }
    citations = citations.append(row, ignore_index=True)

    row = {
        'Source': 'Centers for Disease Control and Prevention',
        'Description': 'Demographics (Manual)',
        'Link': 'https://covid.cdc.gov/covid-data-tracker/#vaccination-demographic',
        'Date Retrieved or Modified': 'Apr 1 2021'
    }
    citations = citations.append(row, ignore_index=True)

    row = {
        'Source': 'Centers for Disease Control and Prevention',
        'Description': 'Guidelines (Manual)',
        'Link': 'https://www.cdc.gov/coronavirus/2019-ncov/index.html',
        'Date Retrieved or Modified': 'Apr 1 2021'
    }
    citations = citations.append(row, ignore_index=True)

    citations.to_csv('data/citations.csv', index=False)
